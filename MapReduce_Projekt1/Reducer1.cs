﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MapReduce_Projekt1
{
    /// <summary>
    /// Contains Reducer logic of the program
    /// </summary>
    public static class Reducer1
    {
        public const string reducer1FileName = "reducer1_";
        public static int FileCount = 0;
        /// <summary>
        /// Reduces mapped words 
        /// </summary>
        /// <param name="mappedWordsCollection"></param>
        private static void ReduceWords(List<string> mappedWordsCollection, int fileCount)
        {
            // Delete file if it resides on disk from previous program calls
            if (File.Exists(reducer1FileName + fileCount))
            {
                File.Delete(reducer1FileName + fileCount);
            }
            var reducedWords = new List<KeyValuePair<string, int>>();
            // Get words from the line - lock makes add operation thread-safe
            foreach (var word in mappedWordsCollection)
            {
                lock (reducedWords) { reducedWords.Add(new KeyValuePair<string, int>(word, 1)); }
            }
            // for all words in given collection
            for (int i = 0; i < reducedWords.Count - 1; i++)
            {
                // if next key is same as this one
                while (reducedWords[i].Key == reducedWords[i + 1].Key)
                {
                    lock (reducedWords)
                    {
                        // Check if we don't want to perform operation on element outside the array index
                        if (i + 1 < reducedWords.Count)
                        {
                            // add +1 to the value of next key and remove current one
                            reducedWords[i + 1] = new KeyValuePair<string, int>(reducedWords[i].Key, reducedWords[i].Value + 1);
                            reducedWords.RemoveAt(i);
                        }
                        // break while loop, if we're at the end of collection
                        if (i + 1 >= reducedWords.Count) break;
                    }
                }             
            }
            // Add to file
            foreach (var item in reducedWords)
            {
                using (StreamWriter file = File.AppendText(reducer1FileName + fileCount))
                {
                    file.WriteLine(item);
                }
            }
        }

        /// <summary>
        /// Shuffles mapped words to match reducer count and returns reduced words
        /// </summary>
        /// <param name="mappedWords"></param>
        /// <param name="reducerCount"></param>
        public static void DivideAndReduce(List<string> mappedWords, int reducerCount)
        {
            // Gets identificators from mapped collection
            var mappedIdentificators = mappedWords.Select(x => x.Split('\\')[0]).ToList();

            // Get the number of words to pass per reducer 
            var wordsCount = mappedIdentificators.Count;
            var wordsPerReducer = Math.Ceiling((double)wordsCount / (double)reducerCount);

            var startIndex = 0;

            List<Task> tasks = new List<Task>();
            // For all reducers
            for (int i = 0; i < reducerCount; i++)
            {
                var duplicateWord = 0;
                // Check if we don't want to perform operation on element outside the array index
                if (reducerCount != 1 && wordsPerReducer + startIndex + 1 <= wordsCount)
                {
                    // Shuffle if there are word duplicates in reducers - only one reducer operaters on same key
                    while (mappedIdentificators[(int)wordsPerReducer + startIndex + duplicateWord - 1] == mappedIdentificators[(int)wordsPerReducer + startIndex + duplicateWord])
                    {
                        duplicateWord += 1;
                        // break while loop, if we're at the end of collection
                        if (wordsPerReducer + startIndex + duplicateWord + 1 >= wordsCount) break;
                    }
                }

                // Run reducers asynchronously
                if (wordsCount - startIndex > (int)wordsPerReducer + duplicateWord)
                {
                    var temp = startIndex;
                    var temp2 = (int)wordsPerReducer + duplicateWord;
                    var temp3 = FileCount;
                    tasks.Add(Task.Run(() => Reducer1.ReduceWords(mappedIdentificators.GetRange(temp, temp2), temp3)));
                    startIndex += (int)wordsPerReducer + duplicateWord;
                    FileCount++;
                }
                else if (startIndex < wordsCount)
                {
                    var temp = startIndex;
                    var temp2 = FileCount;
                    tasks.Add(Task.Run(() => Reducer1.ReduceWords(mappedIdentificators.GetRange(temp, wordsCount - temp), temp2)));
                    startIndex += (int)wordsPerReducer + duplicateWord;
                    FileCount++;
                }
            }
            // Wait for all reducers tasks (threads) to finish
            Task.WaitAll(tasks.ToArray());
        }
    }
}
