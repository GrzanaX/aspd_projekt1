﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MapReduce_Projekt1
{
    /// <summary>
    /// Contains sorting logic of the program
    /// </summary>
    public static class SortMappedWords
    {
        // filename of result file
        private const string MapResultFileName = "MapReduce_MapResult.txt";

        /// <summary>
        /// Sorts mapped words
        /// </summary>
        /// <returns>Sorted Program.SortedMappedWords</returns>
        public static void Sort()
        {
            var tempData = new ConcurrentBag<string>();
            for (int i = 0; i < Map._fileCount; i++)
            {
                using (StreamReader sr = File.OpenText(Map.mapFileName + i))
                {
                    string line = String.Empty;
                    while ((line = sr.ReadLine()) != null)
                    {
                        tempData.Add(line);
                    }
                }
            }
            // write results to console window and to the output file
            using (StreamWriter file = new StreamWriter(MapResultFileName))
            {
                Console.WriteLine("\n*Mapped informations are as follows:");
                file.WriteLine("*Mapped informations are as follows:");
                foreach (var item in tempData)
                {
                    Console.WriteLine(item);
                    file.WriteLine(item);
                }
                Console.WriteLine("-> Total count: {0}", tempData.Count);
                file.WriteLine("-> Total count: {0}", tempData.Count);
                file.WriteLine();
            }
            var list = tempData.OrderByDescending(x => x.Substring(0)).ToList();
            Program.SortedMappedWords = new ConcurrentBag<string>(list);

            // write results to console window and to the output file
            using (StreamWriter file = File.AppendText(MapResultFileName))
            {
                Console.WriteLine("\n*Sorted mapped collection is as follows:");
                file.WriteLine("*Sorted mapped collection is as follows:");
                foreach (var item in Program.SortedMappedWords)
                {
                    Console.WriteLine(item);
                    file.WriteLine(item);
                }
                file.WriteLine();
            }
        }
    }
}
