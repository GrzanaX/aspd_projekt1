﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;

namespace MapReduce_Projekt1
{
    /// <summary>
    /// Contains Map logic of the Program
    /// </summary>
    public static class Map
    {
        public const string mapFileName = "map";
        public static int _fileCount = 0;
        /// <summary>
        /// Gets words from each line in a file
        /// </summary>
        /// <param name="fileText"></param>
        /// <returns>List of lines of words</returns>
        public static List<string> GetLinesOfWords(string fileText)
        {
            using (StreamReader r = new StreamReader(fileText))
            {
                var textLines = new List<string>();
                string textLine;
                while ((textLine = r.ReadLine()) != null)
                {
                    textLines.Add(textLine);
                }
                return textLines;
            }
        }

        /// <summary>
        /// Maps words from a given file
        /// </summary>SortedMappedWords
        /// <param name="subListOfWords"></param>
        private static void MapWords(List<string> subListOfWords, int fileCount)
        {
            // Delete file if it resides on disk from previous program calls
            if (File.Exists(mapFileName + fileCount))
            {
                File.Delete(mapFileName + fileCount);
            }

            // Get words from the line
            foreach (var line in subListOfWords)
            {
                //Sample line is as follows below:
                var sample = "127.0.0.1 - - [03/Oct/2009:15:45:56 +0200] “GET /index.html HTTP/1.1” 200 56 “-” “Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.12) Gecko/2009072221 Iceweasel/3.0.6 (Debian-3.0.6-1)”";
                var splited = line.Split(' ');
                if (splited.Length < 7)
                {
                    Console.WriteLine("Nieprawdiłowy format pliku - plik musi być logiem o postaci: \n {0}",sample);
                    Console.ReadLine();
                    Environment.Exit(13);
                }
                var callingAddress = splited[0]; // take ip address from the beginning
                var identificator = splited[6].Substring(1);  // get the actual site without / sign at beginning
                var result = identificator + "\\" + callingAddress; // concatenate
                // Add to file
                using (StreamWriter file = File.AppendText(mapFileName + fileCount))
                {
                    file.WriteLine(result);
                }
            }
        }

        /// <summary>
        /// Shuffles and maps words to match mappers count
        /// </summary>
        /// <param name="lineOfWordsList"></param>
        /// <param name="mapperCount"></param>
        /// <returns></returns>
        public static void DivideAndMap(List<string> lineOfWordsList, int mapperCount)
        {
            var lineCount = lineOfWordsList.Count; // get count of lines

            var linesPerMapper = Math.Ceiling((double)lineCount / (double)mapperCount);
            var j = 0;          

            // asynchronously maps words on different threads
            List<Task> tasks = new List<Task>();
            for (int i = 0; i < mapperCount && i < lineCount; i++)
            {
                if (lineCount - j > (int)linesPerMapper)
                {
                    var temp = j;
                    var temp2 = _fileCount;
                    tasks.Add(Task.Run(() => Map.MapWords(lineOfWordsList.GetRange(temp, (int)linesPerMapper), temp2)));
                    j += (int)linesPerMapper;
                    _fileCount++;
                }
                else if (j < lineCount)
                {
                    var temp = j;
                    var temp2 = _fileCount;
                    tasks.Add(Task.Run(() => Map.MapWords(lineOfWordsList.GetRange(temp, lineCount - temp), temp2)));
                    j += (int)linesPerMapper;
                    _fileCount++;
                }
            }
            Task.WaitAll(tasks.ToArray());
        }
    }
}
