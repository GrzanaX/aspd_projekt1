﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MapReduce_Projekt1
{
    /// <summary>
    /// Project 1 for ASPD class with dr Rafał Bocian
    /// Check TrescZadania.txt file included to project to get a hang of what this program does
    /// </summary>
    public static class Program
    {
        // Collection to store mapped words, in order to pass them to reducers
        public static ConcurrentBag<string> SortedMappedWords = new ConcurrentBag<string>();

        // Variables to hold number of mappers/reducers provided by user
        private static int _countOfMappers;
        private static int _countOfReducers1;
        private static int _countOfReducers2;

        // Filenames to which we will save results
        private const string IdentificatorsResultFileName = "MapReduce_Identificators.txt";
        private const string CallingAddressesResultFileName = "MapReduce_CallingAddressess.txt";

        /// <summary>
        /// Main program. Gets parameters from the user and starts MapReduce algorithm
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            Console.WriteLine("Podaj pełną ścieżkę do pliku (np. C:\\Temp\\plik.txt):");
            var filename = Console.ReadLine();
            // Call to mapReduce function - checks user input first
            if (File.Exists(filename))
            {
                Console.WriteLine("Podaj liczbę mapperów:");
                var parsingResult = int.TryParse(Console.ReadLine(), out _countOfMappers);
                while (parsingResult == false)
                {
                    Console.WriteLine("Podaj liczbę mapperów: (liczba, np. 1)");
                    parsingResult = int.TryParse(Console.ReadLine(), out _countOfMappers);
                }

                Console.WriteLine("Podaj liczbę reducerów zasobu:");
                parsingResult = int.TryParse(Console.ReadLine(), out _countOfReducers1);
                while (parsingResult == false)
                {
                    Console.WriteLine("Podaj liczbę reducerów zasobu: (liczba, np. 1)");
                    parsingResult = int.TryParse(Console.ReadLine(), out _countOfReducers1);
                }

                Console.WriteLine("Podaj liczbę reducerów adresu wywołującego:");
                parsingResult = int.TryParse(Console.ReadLine(), out _countOfReducers2);
                while (parsingResult == false)
                {
                    Console.WriteLine("Podaj liczbę reducerów adresu wywołującego: (liczba, np. 1)");
                    parsingResult = int.TryParse(Console.ReadLine(), out _countOfReducers2);
                }

                MapReduce(filename);
            }
            else
            {
                Console.WriteLine("Plik nie istnieje");
            }

        }

        /// <summary>
        /// Calls functions in order to map and reduce words from given file input
        /// </summary>
        /// <param name="filename"></param>
        private static void MapReduce(string filename)
        {
            var lineOfWords = Map.GetLinesOfWords(filename);

            Map.DivideAndMap(lineOfWords, _countOfMappers);
            SortMappedWords.Sort();

            Parallel.Invoke(
                () => Reducer1.DivideAndReduce(Program.SortedMappedWords.ToList(), _countOfReducers1),
                () => Reducer2.DivideAndReduce(Program.SortedMappedWords.ToList(), _countOfReducers2)
                );

            Console.WriteLine("\n*Reduced identificators are as follows:");
            var tempData1 = new List<string>();
            for (int i = 0; i < Reducer1.FileCount; i++)
            {
                using (StreamReader sr = File.OpenText(Reducer1.reducer1FileName + i))
                {
                    string line = String.Empty;
                    while ((line = sr.ReadLine()) != null)
                    {
                        tempData1.Add(line);
                    }
                }
            }
            if (File.Exists(Program.IdentificatorsResultFileName))
            {
                File.Delete(Program.IdentificatorsResultFileName);
            }
            // write results to console window and to the output file
            using (StreamWriter file = File.AppendText(Program.IdentificatorsResultFileName))
            {
                foreach (var item in tempData1)
                {
                    Console.WriteLine(item);
                    file.WriteLine(item);
                }
                file.WriteLine();
            }
            Console.WriteLine("\n*Reduced addressess are as follows:");      
            var tempData2 = new List<string>();
            for (int i = 0; i < Reducer2.FileCount; i++)
            {
                using (StreamReader sr = File.OpenText(Reducer2.reducer2FileName + i))
                {
                    string line = String.Empty;
                    while ((line = sr.ReadLine()) != null)
                    {
                        tempData2.Add(line);
                    }
                }
            }
            if (File.Exists(Program.CallingAddressesResultFileName))
            {
                File.Delete(Program.CallingAddressesResultFileName);
            }
            // write results to console window and to the output file
            using (StreamWriter file = File.AppendText(Program.CallingAddressesResultFileName))
            {
                foreach (var item in tempData2)
                {
                    Console.WriteLine(item);
                    file.WriteLine(item);
                }
                file.WriteLine();
            }
        }
    }
}
